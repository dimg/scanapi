FROM python:3.9-alpine

LABEL maintainer="hyperjiang@gmail.com"

RUN pip install scanapi

WORKDIR /app

ENTRYPOINT [ "scanapi" ]
